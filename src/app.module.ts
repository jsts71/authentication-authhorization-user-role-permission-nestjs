import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { GraphQLModule } from '@nestjs/graphql';
import { ApolloDriver, ApolloDriverConfig } from '@nestjs/apollo';
import { UserModule } from './user/user.module';
import { join } from 'path';
import { RoleModule } from './role/role.module';
import { PermissionModule } from './permission/permission.module';
import { OrganizationModule } from './organization/organization.module';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';
import genericConfiguration from './configuration/generic.configuration';
import { TypeOrmConfigService } from './configuration/configuration.service';
import { validate } from './configuration/env.validation';

@Module({
  imports: [
    ConfigModule.forRoot({
      validate,
      load: [genericConfiguration],
      isGlobal: true,
      validationOptions: {
        allowUnknown: false,
        abortEarly: true,
      },
    }),
    TypeOrmModule.forRootAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      useClass: TypeOrmConfigService,
    }),
    GraphQLModule.forRoot<ApolloDriverConfig>({
      driver: ApolloDriver,
      include: [OrganizationModule, PermissionModule, RoleModule, UserModule],
      autoSchemaFile: join(process.cwd(), 'src/schema.gql'),
    }),
    UserModule,
    RoleModule,
    PermissionModule,
    OrganizationModule,
  ],
  controllers: [AppController],
  providers: [AppService, TypeOrmConfigService],
})
export class AppModule { }
