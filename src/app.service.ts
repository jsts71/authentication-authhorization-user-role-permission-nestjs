import { Injectable, OnModuleInit, OnModuleDestroy, BeforeApplicationShutdown, OnApplicationShutdown } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { TypeOrmModuleOptions, TypeOrmOptionsFactory } from '@nestjs/typeorm';

@Injectable()
export class AppService implements OnModuleInit, OnModuleDestroy, BeforeApplicationShutdown, OnApplicationShutdown {

  async onModuleInit() {
    console.log(`The module has been initialized. abir signatured!`);
  }

  async onModuleDestroy() {
    console.log("The module is destroying. abir signatured!");
  }

  beforeApplicationShutdown(signal: string) {
    console.log("The application is shutting down. abir signatured!", signal);
  }

  onApplicationShutdown(signal: string) {
    console.log("The application has shut down. abir signatured!", signal);
  }

  getHello(): string {
    return 'Hello World!';
  }

}