import { Injectable } from "@nestjs/common"
import { ConfigService } from "@nestjs/config"
import { TypeOrmModuleOptions, TypeOrmOptionsFactory } from "@nestjs/typeorm"
import { DatabaseConfigurationInterface } from "./consts";

@Injectable()
export class TypeOrmConfigService implements TypeOrmOptionsFactory {
  constructor(private configService: ConfigService) { }

  createTypeOrmOptions(connectionName?: string): TypeOrmModuleOptions | Promise<TypeOrmModuleOptions> {

    const database = this.configService.get('database')
    let dbConfig: DatabaseConfigurationInterface;

    database === 'postgres' ? dbConfig = this.configService.get<DatabaseConfigurationInterface>('db.postgres') :
      database === 'mysql' ? dbConfig = this.configService.get<DatabaseConfigurationInterface>('db.mysql') : null;

    return {
      type: dbConfig.type,
      host: dbConfig.host,
      port: dbConfig.port,
      username: dbConfig.username,
      password: dbConfig.password,
      database: dbConfig.database,
      synchronize: dbConfig.synchronize,
      autoLoadEntities: true,
    }
  }
}
